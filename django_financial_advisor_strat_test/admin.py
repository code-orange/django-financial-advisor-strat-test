from django.apps import apps
from django.contrib import admin

for model in apps.get_app_config("django_financial_advisor_strat_test").models.values():
    admin.site.register(model)
