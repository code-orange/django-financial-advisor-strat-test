from datetime import datetime

from django.db import models


class StratTestBankAccount(models.Model):
    name = models.CharField(max_length=200, blank=True, null=True)
    cash = models.DecimalField(max_digits=10, decimal_places=2)
    tstamp = models.DateTimeField(default=datetime.now)

    class Meta:
        db_table = "strat_test_bank_account"


class StratTestDepot(models.Model):
    name = models.CharField(max_length=200, blank=True, null=True)
    bank_account = models.ForeignKey(StratTestBankAccount, models.DO_NOTHING)
    tstamp = models.DateTimeField(default=datetime.now)

    class Meta:
        db_table = "strat_test_depot"


class StratTestDepotLog(models.Model):
    depot = models.ForeignKey(StratTestDepot, models.DO_NOTHING)
    symbol = models.CharField(max_length=200)
    shares = models.DecimalField(max_digits=10, decimal_places=2)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    tstamp = models.DateTimeField(default=datetime.now)

    class Meta:
        db_table = "strat_test_depot_log"
